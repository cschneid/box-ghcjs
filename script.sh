#!/bin/bash
sudo apt-get update
sudo apt-get install -y software-properties-common python-software-properties
sudo add-apt-repository -y ppa:hvr/ghc

sudo apt-get update
sudo apt-get install -y cabal-install-1.22 ghc-7.8.4
sudo ln -sf /usr/bin/cabal-1.22 /usr/bin/cabal
sudo ln -sf /opt/ghc/7.8.4/bin/* /usr/bin/

echo 'export PATH="~/.cabal/bin:/opt/cabal/1.22/bin:/opt/ghc/7.8.4/bin:\$PATH"' >> ~/.bashrc
export PATH=~/.cabal/bin:/opt/cabal/1.22/bin:/opt/ghc/7.8.4/bin:$PATH

# Alex & Happy & Cabal 1.22
cabal update
cabal install cabal alex happy

# Node
sudo apt-add-repository -y ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install -y nodejs

# Other junks
sudo apt-get install -y make autoconf git libtinfo-dev

# Actually install ghcjs
git clone https://github.com/ghcjs/ghcjs-prim.git
git clone https://github.com/ghcjs/ghcjs.git
cabal install --reorder-goals --max-backjumps=-1 ./ghcjs ./ghcjs-prim
ghcjs-boot --dev

